<?php
/**
 * @file
 * pronosticos.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pronosticos_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access site-wide contact form'.
  $permissions['access site-wide contact form'] = array(
    'name' => 'access site-wide contact form',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'contact',
  );

  // Exported permission: 'access user contact forms'.
  $permissions['access user contact forms'] = array(
    'name' => 'access user contact forms',
    'roles' => array(),
    'module' => 'contact',
  );

  // Exported permission: 'create partido content'.
  $permissions['create partido content'] = array(
    'name' => 'create partido content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any partido content'.
  $permissions['delete any partido content'] = array(
    'name' => 'delete any partido content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own partido content'.
  $permissions['delete own partido content'] = array(
    'name' => 'delete own partido content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any partido content'.
  $permissions['edit any partido content'] = array(
    'name' => 'edit any partido content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own partido content'.
  $permissions['edit own partido content'] = array(
    'name' => 'edit own partido content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
