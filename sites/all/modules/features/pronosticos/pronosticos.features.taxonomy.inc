<?php
/**
 * @file
 * pronosticos.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function pronosticos_taxonomy_default_vocabularies() {
  return array(
    'equipos' => array(
      'name' => 'Equipos',
      'machine_name' => 'equipos',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
