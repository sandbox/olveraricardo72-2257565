<?php
/**
 * @file
 * pronosticos.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pronosticos_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function pronosticos_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function pronosticos_node_info() {
  $items = array(
    'partido' => array(
      'name' => t('Partido'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'quiniela_fase_de_grupos' => array(
      'name' => t('Quiniela fase de grupos'),
      'base' => 'node_content',
      'description' => t('Contiene los pronósticos de la fase de grupos de un usuario'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
