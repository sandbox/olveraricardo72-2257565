<?php
/**
 * @file
 * pronosticos.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pronosticos_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_calendario:calendario-partidos/mes/2014-06
  $menu_links['main-menu_calendario:calendario-partidos/mes/2014-06'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'calendario-partidos/mes/2014-06',
    'router_path' => 'calendario-partidos/mes',
    'link_title' => 'Calendario',
    'options' => array(
      'attributes' => array(
        'title' => 'Calendario de partidos',
      ),
      'identifier' => 'main-menu_calendario:calendario-partidos/mes/2014-06',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_frontpage:node/175
  $menu_links['main-menu_frontpage:node/175'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/175',
    'router_path' => 'node/%',
    'link_title' => 'frontpage',
    'options' => array(
      'identifier' => 'main-menu_frontpage:node/175',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_inicio:<front>
  $menu_links['main-menu_inicio:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Inicio',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_inicio:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_mi-quiniela:quiniela
  $menu_links['main-menu_mi-quiniela:quiniela'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'quiniela',
    'router_path' => 'quiniela',
    'link_title' => 'Mi quiniela',
    'options' => array(
      'attributes' => array(
        'title' => 'Ver mi quiniela',
      ),
      'identifier' => 'main-menu_mi-quiniela:quiniela',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_podio:podio
  $menu_links['main-menu_podio:podio'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'podio',
    'router_path' => 'podio',
    'link_title' => 'Podio',
    'options' => array(
      'attributes' => array(
        'title' => 'Tres primeros lugares en la quiniela',
      ),
      'identifier' => 'main-menu_podio:podio',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_posiciones:tabla_de_posiciones
  $menu_links['main-menu_posiciones:tabla_de_posiciones'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'tabla_de_posiciones',
    'router_path' => 'tabla_de_posiciones',
    'link_title' => 'Posiciones',
    'options' => array(
      'attributes' => array(
        'title' => 'Puntaje de l@s quinieler@s',
      ),
      'identifier' => 'main-menu_posiciones:tabla_de_posiciones',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_tabla-general:tabla_general
  $menu_links['main-menu_tabla-general:tabla_general'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'tabla_general',
    'router_path' => 'tabla_general',
    'link_title' => 'Tabla general',
    'options' => array(
      'attributes' => array(
        'title' => 'Tabla de posiciones de los equipos',
      ),
      'identifier' => 'main-menu_tabla-general:tabla_general',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Calendario');
  t('Inicio');
  t('Mi quiniela');
  t('Podio');
  t('Posiciones');
  t('Tabla general');
  t('frontpage');


  return $menu_links;
}
