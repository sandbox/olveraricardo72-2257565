<?php
/**
 * @file
 * pronosticos.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function pronosticos_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'partidos';
  $feeds_importer->config = array(
    'name' => 'partidos',
    'description' => 'Importador de partidos',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => 'TAB',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'partido',
        'update_existing' => '1',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'guid',
            'target' => 'nid',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'guid',
            'target' => 'field_numero_de_partido',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'equipo1',
            'target' => 'field_equipo_1',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          5 => array(
            'source' => 'equipo2',
            'target' => 'field_equipo_2',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          6 => array(
            'source' => 'timestamp',
            'target' => 'field_fecha:start',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'estadio',
            'target' => 'field_estadio',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'grupo',
            'target' => 'field_grupo',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '1',
        'authorize' => 0,
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => FALSE,
  );
  $export['partidos'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'quiniela_personal';
  $feeds_importer->config = array(
    'name' => 'Quiniela personal',
    'description' => 'Herramienta importadora de la quiniela de una persona',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'quiniela_fase_de_grupos',
        'update_existing' => '1',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'numero_de_partido',
            'target' => 'field_numero_de_partido',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'marcador_1',
            'target' => 'field_marcador_1',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'marcador_2',
            'target' => 'field_marcador_2',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'username',
            'target' => 'user_name',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '1',
        'authorize' => 0,
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => FALSE,
  );
  $export['quiniela_personal'] = $feeds_importer;

  return $export;
}
