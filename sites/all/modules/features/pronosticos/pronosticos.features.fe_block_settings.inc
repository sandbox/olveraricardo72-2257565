<?php
/**
 * @file
 * pronosticos.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pronosticos_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-corporateclean_credits'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'corporateclean_credits',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'footer_third',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'likable_clean_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'likable_clean_theme',
        'weight' => 0,
      ),
      'marinelli' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'marinelli',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 0,
      ),
      'white' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'white',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-foot_rof'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'foot_rof',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'footer_first',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'likable_clean_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'likable_clean_theme',
        'weight' => 0,
      ),
      'marinelli' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'marinelli',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 0,
      ),
      'white' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'white',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-podio'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'podio',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'likable_clean_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'likable_clean_theme',
        'weight' => 0,
      ),
      'marinelli' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'marinelli',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 0,
      ),
      'white' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'white',
        'weight' => 0,
      ),
    ),
    'title' => 'Podio',
    'visibility' => 1,
  );

  $export['block-tabla_de_posiciones'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'tabla_de_posiciones',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 1,
      ),
      'likable_clean_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'likable_clean_theme',
        'weight' => 1,
      ),
      'marinelli' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'marinelli',
        'weight' => 1,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 0,
      ),
      'white' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'white',
        'weight' => 1,
      ),
    ),
    'title' => 'Posiciones de quinieler@s',
    'visibility' => 1,
  );

  $export['block-tabla_general'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'tabla_general',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 3,
      ),
      'likable_clean_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'likable_clean_theme',
        'weight' => 2,
      ),
      'marinelli' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'marinelli',
        'weight' => 3,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 0,
      ),
      'white' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'white',
        'weight' => 3,
      ),
    ),
    'title' => 'Tabla general de posiciones de los equipos',
    'visibility' => 1,
  );

  $export['calendar-calendar_legend'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'calendar_legend',
    'module' => 'calendar',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'likable_clean_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'likable_clean_theme',
        'weight' => 13,
      ),
      'marinelli' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'marinelli',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 0,
      ),
      'white' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'white',
        'weight' => 0,
      ),
    ),
    'title' => 'Calendario',
    'visibility' => 0,
  );

  $export['system-powered-by'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'powered-by',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'footer_second',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 10,
      ),
      'likable_clean_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'likable_clean_theme',
        'weight' => 10,
      ),
      'marinelli' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'marinelli',
        'weight' => 10,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 10,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 10,
      ),
      'white' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'white',
        'weight' => 10,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-calendario_partidos-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'calendario_partidos-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'likable_clean_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'likable_clean_theme',
        'weight' => 0,
      ),
      'marinelli' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'marinelli',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 0,
      ),
      'white' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'white',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-calendario_partidos-block_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'calendario_partidos-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'likable_clean_theme' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'likable_clean_theme',
        'weight' => 0,
      ),
      'marinelli' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'marinelli',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => -11,
      ),
      'stark' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stark',
        'weight' => 0,
      ),
      'white' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'white',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
