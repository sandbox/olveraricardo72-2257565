<?php
/**
 * @file
 * pronosticos.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function pronosticos_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Corporateclean creadits';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'corporateclean_credits';
  $fe_block_boxes->body = '    <!-- #credits -->
    <div id="credits">
        <p>Ported to Drupal by <a href="http://www.drupalizing.com">Drupalizing</a>, a Project of <a href="http://www.morethanthemes.com">More than (just) Themes</a>. Designed by <a href="http://www.kaolti.com/">Zsolt Kacso</a></p>
    </div>
    <!-- EOF: #credits -->
';

  $export['corporateclean_credits'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'My footer ';
  $fe_block_boxes->format = 'plain_text';
  $fe_block_boxes->machine_name = 'foot_rof';
  $fe_block_boxes->body = 'Don\'t want to play with me? This is a Drupal Distro, ask me how to set up your own site.';

  $export['foot_rof'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Tres primeros lugares con foto';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'podio';
  $fe_block_boxes->body = '<?php
return pronosticos_podio();
?>';

  $export['podio'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'tabla_de_posiciones';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'tabla_de_posiciones';
  $fe_block_boxes->body = '<?php
return pronosticos_tabla_de_posiciones();
?>';

  $export['tabla_de_posiciones'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'tabla_general';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'tabla_general';
  $fe_block_boxes->body = '<?php
return pronosticos_tabla_general();
?>';

  $export['tabla_general'] = $fe_block_boxes;

  return $export;
}
