<?php
/**
 * @file
 * pronosticos.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pronosticos_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_partido';
  $strongarm->value = 'edit-display';
  $export['additional_settings__active_tab_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_partido';
  $strongarm->value = '1';
  $export['ant_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_pattern_partido';
  $strongarm->value = '<?php
  $marcador_1 = count($node->field_marcador_1);
  $marcador_2 = count($node->field_marcador_2);
  if (empty($marcador_1) || empty($marcador_2)) {
    return \'[node:field_equipo_1] - [node:field_equipo_2]\';
  }
  else {
    return \'[node:field_equipo_1] - [node:field_equipo_2] : [node:field_marcador_1] - [node:field_marcador_2]\';
  } 
?>
';
  $export['ant_pattern_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_pattern_quiniela_fase_de_grupos';
  $strongarm->value = 'Quiniela de [node:author:name]';
  $export['ant_pattern_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_php_partido';
  $strongarm->value = 1;
  $export['ant_php_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_php_quiniela_fase_de_grupos';
  $strongarm->value = 0;
  $export['ant_php_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_quiniela_fase_de_grupos';
  $strongarm->value = '1';
  $export['ant_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'calendar_add_colorbox';
  $strongarm->value = '1';
  $export['calendar_add_colorbox'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'clean_url';
  $strongarm->value = 0;
  $export['clean_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_corporateclean_files';
  $strongarm->value = array(
    0 => 'public://color/corporateclean-df5fd4ed/logo.png',
    1 => 'public://color/corporateclean-df5fd4ed/colors.css',
  );
  $export['color_corporateclean_files'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_corporateclean_logo';
  $strongarm->value = 'public://color/corporateclean-df5fd4ed/logo.png';
  $export['color_corporateclean_logo'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_corporateclean_palette';
  $strongarm->value = array(
    'base' => '#7a7a7a',
    'link' => '#319014',
    'headingshadow' => '#eaf1ea',
    'slogan' => '#d5d5d5',
    'headertop' => '#2f2f2f',
    'headerbottom' => '#1b1b1a',
    'headermenu' => '#62b25d',
    'headermenulink' => '#f5f32f',
    'headermenuborder' => '#353535',
    'headermenubghover' => '#1b1b1b',
    'bannertop' => '#efeeee',
    'bannerbottom' => '#dadada',
    'bannerborder' => '#fcfcfc',
    'contenttop' => '#e8e8e8',
    'contentbottom' => '#efefef',
    'blockbg' => '#fdfdfd',
    'buttontop' => '#1dc500',
    'buttonbottom' => '#16a200',
    'buttontext' => '#fefefe',
    'buttontextshadow' => '#0a4700',
    'buttonboxshadow' => '#a2a293',
    'buttonbghover' => '#19b800',
    'footer' => '#181818',
    'footerlink' => '#e4e4e4',
    'footerbottomtop' => '#262626',
    'footerbottombottom' => '#1a1a1a',
  );
  $export['color_corporateclean_palette'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_corporateclean_stylesheets';
  $strongarm->value = array(
    0 => 'public://color/corporateclean-df5fd4ed/colors.css',
  );
  $export['color_corporateclean_stylesheets'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_partido';
  $strongarm->value = 0;
  $export['comment_anonymous_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_quiniela_fase_de_grupos';
  $strongarm->value = 0;
  $export['comment_anonymous_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_partido';
  $strongarm->value = 1;
  $export['comment_default_mode_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_quiniela_fase_de_grupos';
  $strongarm->value = 1;
  $export['comment_default_mode_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_partido';
  $strongarm->value = '50';
  $export['comment_default_per_page_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_quiniela_fase_de_grupos';
  $strongarm->value = '50';
  $export['comment_default_per_page_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_partido';
  $strongarm->value = 1;
  $export['comment_form_location_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_quiniela_fase_de_grupos';
  $strongarm->value = 1;
  $export['comment_form_location_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_partido';
  $strongarm->value = '2';
  $export['comment_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_partido';
  $strongarm->value = '1';
  $export['comment_preview_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_quiniela_fase_de_grupos';
  $strongarm->value = '1';
  $export['comment_preview_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_quiniela_fase_de_grupos';
  $strongarm->value = '2';
  $export['comment_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_partido';
  $strongarm->value = 1;
  $export['comment_subject_field_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_quiniela_fase_de_grupos';
  $strongarm->value = 1;
  $export['comment_subject_field_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'contact_default_status';
  $strongarm->value = 1;
  $export['contact_default_status'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_field_collection_item__field_partido';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_field_collection_item__field_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__partido';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '1',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__quiniela_fase_de_grupos';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '1',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_partido';
  $strongarm->value = '0';
  $export['language_content_type_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_quiniela_fase_de_grupos';
  $strongarm->value = '0';
  $export['language_content_type_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_partido';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_quiniela_fase_de_grupos';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_partido';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_quiniela_fase_de_grupos';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_panel';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_panel'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_partido';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_quiniela_fase_de_grupos';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_partido';
  $strongarm->value = '1';
  $export['node_preview_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_quiniela_fase_de_grupos';
  $strongarm->value = '1';
  $export['node_preview_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_partido';
  $strongarm->value = 0;
  $export['node_submitted_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_quiniela_fase_de_grupos';
  $strongarm->value = 1;
  $export['node_submitted_quiniela_fase_de_grupos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panels_node_default';
  $strongarm->value = array(
    'entity_field_extra' => 'entity_field_extra',
    'entity_field' => 'entity_field',
    'block' => 'block',
    'entity_form_field' => 'entity_form_field',
    'token' => 'token',
    'custom' => 'custom',
    'entity_view' => 'entity_view',
    'panels_mini' => 'panels_mini',
    'other' => 'other',
  );
  $export['panels_node_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_partido_pattern';
  $strongarm->value = 'partido/[node:title]';
  $export['pathauto_node_partido_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_pattern';
  $strongarm->value = 'content/[node:title]';
  $export['pathauto_node_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_quiniela_fase_de_grupos_pattern';
  $strongarm->value = '';
  $export['pathauto_node_quiniela_fase_de_grupos_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_etapa';
  $strongarm->value = '1';
  $export['quiniela_etapa'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_puntos_equipo_a_cuartos';
  $strongarm->value = '11';
  $export['quiniela_puntos_equipo_a_cuartos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_puntos_equipo_a_octavos';
  $strongarm->value = '10';
  $export['quiniela_puntos_equipo_a_octavos'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_puntos_equipo_a_semifinales';
  $strongarm->value = '12';
  $export['quiniela_puntos_equipo_a_semifinales'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_puntos_equipo_campeon';
  $strongarm->value = '13';
  $export['quiniela_puntos_equipo_campeon'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_puntos_ganador_sin_marcadores';
  $strongarm->value = '3';
  $export['quiniela_puntos_ganador_sin_marcadores'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_puntos_ganador_un_marcador';
  $strongarm->value = '4';
  $export['quiniela_puntos_ganador_un_marcador'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_puntos_marcadores';
  $strongarm->value = '5';
  $export['quiniela_puntos_marcadores'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'quiniela_puntos_sin_ganador_un_marcador';
  $strongarm->value = '0';
  $export['quiniela_puntos_sin_ganador_un_marcador'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'save_continue_partido';
  $strongarm->value = 'Save and add fields';
  $export['save_continue_partido'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_403';
  $strongarm->value = '';
  $export['site_403'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_404';
  $strongarm->value = '';
  $export['site_404'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_default_country';
  $strongarm->value = 'MX';
  $export['site_default_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'contact';
  $export['site_frontpage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_mail';
  $strongarm->value = 'olveraricardo72@gmail.com';
  $export['site_mail'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_name';
  $strongarm->value = 'Brazil 2014';
  $export['site_name'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_slogan';
  $strongarm->value = '';
  $export['site_slogan'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_corporateclean_settings';
  $strongarm->value = array(
    'toggle_logo' => 0,
    'toggle_name' => 0,
    'toggle_slogan' => 0,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 0,
    'toggle_secondary_menu' => 0,
    'default_logo' => 0,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'breadcrumb_display' => 1,
    'breadcrumb_separator' => '/',
    'breadcrumb_home' => 1,
    'slideshow_display' => 1,
    'slideshow_js' => 1,
    'slideshow_effect' => 'scrollUp',
    'slideshow_effect_time' => '10',
    'slideshow_randomize' => 0,
    'slideshow_wrap' => 1,
    'slideshow_pause' => 1,
    'responsive_meta' => 1,
    'responsive_respond' => 0,
    'scheme' => '',
    'palette' => array(
      'base' => '#7a7a7a',
      'link' => '#319014',
      'headingshadow' => '#eaf1ea',
      'slogan' => '#d5d5d5',
      'headertop' => '#2f2f2f',
      'headerbottom' => '#1b1b1a',
      'headermenu' => '#62b25d',
      'headermenulink' => '#f5f32f',
      'headermenuborder' => '#353535',
      'headermenubghover' => '#1b1b1b',
      'bannertop' => '#efeeee',
      'bannerbottom' => '#dadada',
      'bannerborder' => '#fcfcfc',
      'contenttop' => '#e8e8e8',
      'contentbottom' => '#efefef',
      'blockbg' => '#fdfdfd',
      'buttontop' => '#1dc500',
      'buttonbottom' => '#16a200',
      'buttontext' => '#fefefe',
      'buttontextshadow' => '#0a4700',
      'buttonboxshadow' => '#a2a293',
      'buttonbghover' => '#19b800',
      'footer' => '#181818',
      'footerlink' => '#e4e4e4',
      'footerbottomtop' => '#262626',
      'footerbottombottom' => '#1a1a1a',
    ),
    'theme' => 'corporateclean',
    'info' => array(
      'fields' => array(
        'base' => 'Text color',
        'link' => 'Headings/Link',
        'headingshadow' => 'Headings shadow',
        'slogan' => 'Slogan',
        'headertop' => 'Header top',
        'headerbottom' => 'Header bottom',
        'headermenu' => 'Main menu strip',
        'headermenulink' => 'Main menu link',
        'headermenuborder' => 'Main menu/Footer menu border',
        'headermenubghover' => 'Main menu background hover (>=2 level)',
        'bannertop' => 'Banner top',
        'bannerbottom' => 'Banner bottom',
        'bannerborder' => 'Banner border',
        'contenttop' => 'Content top',
        'contentbottom' => 'Content bottom',
        'blockbg' => 'Block background',
        'blockshadow' => 'Block shadow',
        'buttontop' => 'Button top',
        'buttonbottom' => 'Button bottom',
        'buttontext' => 'Button text',
        'buttontextshadow' => 'Button text shadow',
        'buttonboxshadow' => 'Button box shadow',
        'buttonbghover' => 'Button background hover',
        'footer' => 'Footer 1',
        'footerlink' => 'Footer link',
        'footerbottomtop' => 'Footer 2 top',
        'footerbottombottom' => 'Footer 2 bottom',
      ),
      'schemes' => array(
        'default' => array(
          'title' => 'Default',
          'colors' => array(
            'base' => '#7a7a7a',
            'link' => '#1487d4',
            'headingshadow' => '#eaf1ea',
            'slogan' => '#d5d5d5',
            'headertop' => '#2f2f2f',
            'headerbottom' => '#1b1a1a',
            'headermenu' => '#222222',
            'headermenulink' => '#ffffff',
            'headermenuborder' => '#353535',
            'headermenubghover' => '#1b1b1b',
            'bannertop' => '#efeeee',
            'bannerbottom' => '#dadada',
            'bannerborder' => '#fcfcfc',
            'contenttop' => '#e8e8e8',
            'contentbottom' => '#efefef',
            'blockbg' => '#fdfdfd',
            'buttontop' => '#0093c5',
            'buttonbottom' => '#0079a2',
            'buttontext' => '#fefefe',
            'buttontextshadow' => '#003547',
            'buttonboxshadow' => '#939da2',
            'buttonbghover' => '#008ab8',
            'footer' => '#181818',
            'footerlink' => '#e4e4e4',
            'footerbottomtop' => '#262626',
            'footerbottombottom' => '#1a1a1a',
          ),
        ),
        'alignment' => array(
          'title' => 'Alignment',
          'colors' => array(
            'base' => '#7a7a7a',
            'link' => '#de6a00',
            'headingshadow' => '#ffffff',
            'slogan' => '#d5d5d5',
            'headertop' => '#222222',
            'headerbottom' => '#203e42',
            'headermenu' => '#222222',
            'headermenulink' => '#d4fffd',
            'headermenuborder' => '#222222',
            'headermenubghover' => '#203e42',
            'bannertop' => '#efeeee',
            'bannerbottom' => '#dadada',
            'bannerborder' => '#fcfcfc',
            'contenttop' => '#e8e8e8',
            'contentbottom' => '#efefef',
            'blockbg' => '#fdfdfd',
            'buttontop' => '#db9655',
            'buttonbottom' => '#de6a00',
            'buttontext' => '#fefefe',
            'buttontextshadow' => '#222222',
            'buttonboxshadow' => '#999999',
            'buttonbghover' => '#de6a00',
            'footer' => '#203e42',
            'footerlink' => '#d4fffd',
            'footerbottomtop' => '#203e42',
            'footerbottombottom' => '#222222',
          ),
        ),
        '' => array(
          'title' => 'Custom',
          'colors' => array(),
        ),
      ),
      'css' => array(
        0 => 'color/colors.css',
      ),
      'copy' => array(
        0 => 'logo.png',
      ),
      'gradients' => array(
        0 => array(
          'dimension' => array(
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
          ),
          'direction' => 'vertical',
          'colors' => array(
            0 => 'top',
            1 => 'bottom',
          ),
        ),
      ),
      'fill' => array(),
      'slices' => array(),
      'blend_target' => '#ffffff',
      'preview_css' => 'color/preview.css',
      'preview_js' => 'color/preview.js',
      'preview_html' => 'color/preview.html',
      'base_image' => 'color/base.png',
    ),
  );
  $export['theme_corporateclean_settings'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_default';
  $strongarm->value = 'corporateclean';
  $export['theme_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_pictures';
  $strongarm->value = 1;
  $export['user_pictures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_picture_default';
  $strongarm->value = 'sites/default/files/pictures/default-user.jpg';
  $export['user_picture_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_picture_dimensions';
  $strongarm->value = '220x220';
  $export['user_picture_dimensions'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_picture_file_size';
  $strongarm->value = '100';
  $export['user_picture_file_size'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_picture_guidelines';
  $strongarm->value = '';
  $export['user_picture_guidelines'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_picture_path';
  $strongarm->value = 'pictures';
  $export['user_picture_path'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_picture_style';
  $strongarm->value = 'medium';
  $export['user_picture_style'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_node';
  $strongarm->value = array(
    'partido' => 'partido',
    'date_basura' => 0,
    'panel' => 0,
    'poll' => 0,
    'posiciones' => 0,
    'quiniela_fase_de_grupos' => 0,
  );
  $export['uuid_features_entity_node'] = $strongarm;

  return $export;
}
