<?php
/**
 * @file
 * equipos.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function equipos_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => '1F',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '05e4f52a-e510-4f0f-8c5d-751ccf86c1c2',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W54',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '077cb4d9-c035-48e4-ac1c-39a6fe4d0f31',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '2A',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0a10df65-ffba-4b35-9ceb-5caf66287d64',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Greece',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '16084ef6-5767-45d6-a5f2-964481454ad3',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Portugal',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1c81212b-6806-455e-b58e-af728c842a80',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Italy',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1cd8024d-2df1-48a9-a661-9e4e271df9bb',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Chile',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1cede39d-cf7c-407e-a363-206a16859e9f',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Cameroon',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1e15f0a1-dc61-464c-a3f0-05d32c9e5f4a',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W49',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '21afcb7e-a97f-498e-a692-36846dd22aae',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'England',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '268dc4b9-9235-4e78-947f-d0900162e4ab',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W69',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '27ea0698-9749-454b-befc-13727fed0729',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '1H',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2876cba1-d6a7-44fe-959a-2c306540f859',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '1D',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2c211c26-732b-4edd-93f4-7b7a9706ecb7',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '2B',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4518e673-3baa-46bf-a9c8-e67dc81464d1',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'L62',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '47832506-fc7d-4d71-a9e0-a98bf9037eed',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '2H',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4e4a6e5f-54c7-4eca-b2dd-f104a4b73834',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W59',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4ef964c3-ddc5-41df-8e1f-20f87a38f274',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Côte d\'Ivoire',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5a3acab3-6774-4be3-b150-bcd3ad78ac29',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '1C',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5a42076d-74ca-43dd-9969-85aa3d604901',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W61',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '60b7643c-b2aa-4fa9-9f5e-ec966d2a76f9',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W55',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '62946dd6-9825-457d-add3-ed8df3e12d3d',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Uruguay',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '640dbf23-7c51-4a1e-a4ba-8b9e5d22bfa2',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '1G',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6a7da759-1226-4375-ae0e-9becb4e22ad7',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '2G',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '732315f7-1fcb-437b-b85f-1f48281a4d89',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Ghana',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '736f186b-6243-4dfb-a975-6d4d13620447',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Switzerland',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '753062d1-0923-47a5-b000-bf217c142cd2',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '2C',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7802dc0c-9f24-4967-b668-c9dd0c7c10e3',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Colombia',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7817822e-3282-40cc-a040-75eb61e9eab1',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'South Korea',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '789d7a11-0edb-4191-a944-290e65bb5265',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Japan',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7974db98-b9df-4907-83d6-91c7fa48908e',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Australia',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '79a878f6-970f-40c4-93ec-79fd4fd5c3b9',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Netherlands',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7e84470a-7eee-4639-8bee-57c8ffe1b450',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W52',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '850b5a73-672d-48d6-9c2c-77a68d60d337',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Ecuador',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8725962e-4384-47fd-bfa4-5e539923cdff',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Nigeria',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '87bc154c-2368-4cf4-bf8c-4cc0e9892bf0',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '2F',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8cf10a6f-527f-43ec-8f3b-462af40e31d1',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Brazil',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8f94c7ce-72ed-40b7-8e26-3187eb021136',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Iran',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8f95704c-a545-46da-9dfe-8c49b5788ac2',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '2E',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '908b1b83-2dd4-4828-bcb3-5825e6fbd782',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W51',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '944d5986-5251-4eb2-a10a-b786b796223f',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '2D',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9d4653de-dfd4-4a75-b22b-e9e0985a7373',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Mexico',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a1e9cfbf-b9f4-494b-b32c-4023de1ceb94',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '1E',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a288c338-b60a-4251-9627-f5e634595c16',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W57',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a43ef9e1-9b29-4477-8c34-b39dc23b3f53',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Honduras',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a4af99f0-7acb-45ea-937a-a3500ed68702',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Croatia',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'aea126ce-9468-43e6-ba87-4b5d946d2d5f',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Belgium',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'cc147b81-b9ce-436a-beff-d73447eee8d4',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Bosnia-Herzegovina',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd282c1f7-aa5b-4adb-a54f-3c73d46bdf43',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W50',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd367b28e-589c-42c3-9a20-4882b9602515',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Costa Rica',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'da411b00-fd80-4025-b596-73b27ee12870',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'France',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'dade2471-71ae-4814-946f-ffecbf2467a9',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Germany',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'dbf13641-2336-45a2-93fe-a7e17dcc9111',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '1A',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'dc5bafa8-5ed2-400b-a621-7fc9dbb3bdd2',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => '1B',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'dd83c574-d943-4d20-88b0-71dff93eea4f',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'L61',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'dd90c496-d9e7-4ca6-b37b-28410738d3d0',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W62',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e1716496-6b11-4c55-92ae-2c5505fe845a',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'United States',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e3b6a304-2fea-42ec-9019-b487310af8ee',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W58',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e401915d-8d8d-415c-87ab-a3c61643e276',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W53',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e7e16a86-8f30-484c-97ea-34e677ec861e',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Argentina',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ebe96c8a-f8c4-4c40-b92f-26cfe970bd18',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Russia',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ec1fe1f4-d16f-480b-ae57-3225c231a6fd',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Spain',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ef80a77f-1e51-4a26-9bd1-6121ec682952',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'W56',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'f90dca28-cd87-4c15-9296-7b2a6592648f',
    'vocabulary_machine_name' => 'equipos',
  );
  $terms[] = array(
    'name' => 'Algeria',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'f9d38083-08c3-4556-822a-ca510d13add7',
    'vocabulary_machine_name' => 'equipos',
  );
  return $terms;
}
