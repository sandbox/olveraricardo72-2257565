<?php
/**
 * @file
 * equipos.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function equipos_taxonomy_default_vocabularies() {
  return array(
    'equipos' => array(
      'name' => 'Equipos',
      'machine_name' => 'equipos',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
