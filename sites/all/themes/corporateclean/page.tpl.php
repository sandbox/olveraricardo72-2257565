<!-- #header -->
<div id="header">
	<!-- #header-inside -->
    <div id="header-inside" class="container_12 clearfix">
    	<!-- #header-inside-left -->
        <div id="header-inside-left" class="grid_8">

            <?php if ($logo): ?>
            <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
            <?php endif; ?>

            <?php if ($site_name || $site_slogan): ?>
            <div class="clearfix">
            <?php if ($site_name): ?>
            <span id="site-name"><a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></span>
            <?php endif; ?>
            <?php if ($site_slogan): ?>
            <span id="slogan"><?php print $site_slogan; ?></span>
            <?php endif; ?>
            </div>
            <?php endif; ?>

        </div><!-- EOF: #header-inside-left -->

        <!-- #header-inside-right -->
        <div id="header-inside-right" class="grid_4">

			<?php print render($page['search_area']); ?>

        </div><!-- EOF: #header-inside-right -->

    </div><!-- EOF: #header-inside -->

</div><!-- EOF: #header -->

<!-- #header-menu -->
<div id="header-menu">
	<!-- #header-menu-inside -->
    <div id="header-menu-inside" class="container_12 clearfix">

    	<div class="grid_12">
            <div id="navigation" class="clearfix">
            <?php if ($page['navigation']) :?>
            <?php print drupal_render($page['navigation']); ?>
            <?php else :
            if (module_exists('i18n_menu')) {
            $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
            } else {
            $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
            }
            print drupal_render($main_menu_tree);
            endif; ?>
            </div>
        </div>

    </div><!-- EOF: #header-menu-inside -->

</div><!-- EOF: #header-menu -->

<!-- #banner -->
<div id="banner">

	<?php print render($page['banner']); ?>

    <?php if (theme_get_setting('slideshow_display','corporateclean')): ?>

    <?php if ($is_front): ?>

    <!-- #slider-controls-wrapper -->
    <div id="slider-controls-wrapper">
        <div id="slider-controls" class="container_12">
            <ul id="slider-navigation">
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>
        </div>
    </div>
    <!-- EOF: #slider-controls-wrapper -->

    <!-- #slideshow -->
    <div id="slideshow">

        <!--slider-item-->
        <div class="slider-item">
            <div class="content container_12">
            	<div class="grid_12">

                <!--slider-item content-->
                <div style="float:left; padding:0 30px 0 0;">
                <img class="masked" src="<?php print base_path() . drupal_get_path('theme', 'corporateclean') ;?>/mockup/192px-WC-2014-Brasil.svg.png"/>
                </div>
                <h2>Bienveni@ al mundial</h2>
<!--                <strong>Quiniela</strong><br/>
                <em>Client name</em><br/>-->
                <br/>
                <p>Esta quiniela entre amig@s cuesta $100 pesos mexicanos para la fase de grupos y se premia al finalizar ésta.</p>
		<ul>
		<li>50% de las entradas al primer lugar,</li>
		<li>30% al segundo y</li>
		<li>20% al tercer lugar.</li>
		</ul>
		<p>En caso de empates se reparte el monto entre quienes empataron.</p>
                <p>La puntuación es 5 puntos por acertar un marcador, 4 por acertar el equipo ganador y uno de los marcadores y 3 por acertar al equipo ganador.</p>
		<h3>¿Cómo puedo participar?</h3>
		<ul>
		<li>1. Realiza tu pago.</li>
		<li>2. Elige tu nombre de usuario y envía una foto para tu perfil.</li>
		<li>3. Llena tu quiniela.</li>
		</ul>
                <!--EOF:slider-item content-->

                </div>
            </div>
        </div>
        <!--EOF:slider-item-->

        <!--slider-item-->
        <div class="slider-item">
            <div class="content container_12">
            	<div class="grid_12">

                <!--slider-item content-->
                <div style="float:right; padding:0 0 0 30px;">
                <img class="masked" src="<?php print base_path() . drupal_get_path('theme', 'corporateclean') ;?>/mockup/quiniela.png"/>
                </div>
                <h2>Llena tu quiniela</h2>
<!--                <strong>General</strong><br/>
                <em>Client name</em><br/>-->
                <br/>
                Si ya realizaste tu pago ahora puedes llenar tu quiniela.
                <div style="display:block; padding:30px 0 10px 0;"><a class="more" href="node/add/quiniela-fase-de-grupos">Llénala</a></div>
                <!--EOF:slider-item content-->

				</div>
            </div>
        </div>
        <!--EOF:slider-item-->

        <!--slider-item-->
        <div class="slider-item">
            <div class="content container_12">
            	<div class="grid_12">

                <!--slider-item content-->
                <h2>Posiciones en la quiniela</h2>
                <strong>And the winner is...</strong><br/>
                <em>¿Quién?</em><br/>
                <br/>
		<?php print render(pronosticos_tabla_de_posiciones(5)); ?>

                <div style="display:block; padding:30px 0 10px 0;"><a class="more" href="tabla_de_posiciones">Ver la tabla completa</a></div>
                <!--EOF:slider-item content-->

				</div>
            </div>
        </div>
        <!--EOF:slider-item-->

        <div class="slider-item">
            <div class="content container_12">
            	<div class="grid_12">

                <!--slider-item content-->
                <h2>Podio</h2>
		<?php print render(pronosticos_podio());?>
                </div>
                <!--EOF:slider-item content-->

                </div>
            </div>
        </div>
        <!--EOF:slider-item-->

    </div>
    <!-- EOF: #slideshow -->

    <?php endif; ?>

	<?php endif; ?>

</div><!-- EOF: #banner -->


<!-- #content -->
<div id="content">
	<!-- #content-inside -->
    <div id="content-inside" class="container_12 clearfix">

        <?php if ($page['sidebar_first']) :?>
        <!-- #sidebar-first -->
        <div id="sidebar-first" class="grid_4">
        	<?php print render($page['sidebar_first']); ?>
        </div><!-- EOF: #sidebar-first -->
        <?php endif; ?>

        <?php if ($page['sidebar_first'] && $page['sidebar_second']) { ?>
        <div class="grid_4">
        <?php } elseif ($page['sidebar_first'] || $page['sidebar_second']) { ?>
        <div id="main" class="grid_8">
		<?php } else { ?>
        <div id="main" class="grid_12">
        <?php } ?>

            <?php if (theme_get_setting('breadcrumb_display','corporateclean')): print $breadcrumb; endif; ?>

            <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>

            <?php if ($messages): ?>
            <div id="console" class="clearfix">
            <?php print $messages; ?>
            </div>
            <?php endif; ?>

            <?php if ($page['help']): ?>
            <div id="help">
            <?php print render($page['help']); ?>
            </div>
            <?php endif; ?>

            <?php if ($action_links): ?>
            <ul class="action-links">
            <?php print render($action_links); ?>
            </ul>
            <?php endif; ?>

			<?php print render($title_prefix); ?>
            <?php if ($title): ?>
            <h1><?php print $title ?></h1>
            <?php endif; ?>
            <?php print render($title_suffix); ?>

            <?php if ($tabs): ?><?php print render($tabs); ?><?php endif; ?>

            <?php print render($page['content']); ?>

            <?php print $feed_icons; ?>

        </div><!-- EOF: #main -->

        <?php if ($page['sidebar_second']) :?>
        <!-- #sidebar-second -->
        <div id="sidebar-second" class="grid_4">
        	<?php print render($page['sidebar_second']); ?>
        </div><!-- EOF: #sidebar-second -->
        <?php endif; ?>

    </div><!-- EOF: #content-inside -->

</div><!-- EOF: #content -->

<!-- #footer -->
<div id="footer">
	<!-- #footer-inside -->
    <div id="footer-inside" class="container_12 clearfix">

        <div class="footer-area grid_4">
        <?php print render($page['footer_first']); ?>
        </div><!-- EOF: .footer-area -->

        <div class="footer-area grid_4">
        <?php print render($page['footer_second']); ?>
        </div><!-- EOF: .footer-area -->

        <div class="footer-area grid_4">
        <?php print render($page['footer_third']); ?>
        </div><!-- EOF: .footer-area -->

    </div><!-- EOF: #footer-inside -->

</div><!-- EOF: #footer -->

<!-- #footer-bottom -->
<div id="footer-bottom">

	<!-- #footer-bottom-inside -->
    <div id="footer-bottom-inside" class="container_12 clearfix">
    	<!-- #footer-bottom-left -->
    	<div id="footer-bottom-left" class="grid_8">

            <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('class' => array('secondary-menu', 'links', 'clearfix')))); ?>

            <?php print render($page['footer']); ?>

        </div>
    	<!-- #footer-bottom-right -->
        <div id="footer-bottom-right" class="grid_4">

        	<?php print render($page['footer_bottom_right']); ?>

        </div><!-- EOF: #footer-bottom-right -->

    </div><!-- EOF: #footer-bottom-inside -->

</div><!-- EOF: #footer -->